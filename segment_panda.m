% SEGMENT_PANDA contains the implementation of the main routine for Assignment 2. 
% This routine reads a image, which contains four intensity classes.
% The routine employs the Expectation-maximization method to estimate the parameters
% of the four intensity classes with a mixture of four Gaussian distributions, and
% segment the image with minimum error thresholds.
%  
function segment_panda() 

% Define convergence threshold.
threshold = 0.01;

% Read the panda image and convert the color image into grayscale image.
Im = imread('panda.jpg');
Im = rgb2gray(Im);
% Build a histgoram of the image, it is for the sake of
% parameter estimations and visualization.
Hist = imhist(Im,256)';

%
% The Expectation-maximization algorithm.
%

% Initialize the paramters.
Weight = zeros(4,1);
Mu = zeros(4,1);
Sigma = zeros(4,1);
Weight(1) = 0.35;
Weight(2) = 0.25;
Weight(3) = 0.25;
Weight(4) = 0.15;
Mu(1) = 5;
Mu(2) = 60;
Mu(3) = 90;
Mu(4) = 230;
Sigma(1) = 1
Sigma(2) = 10;
Sigma(3) = 10;
Sigma(4) = 20;

itn = 1;
while(1)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% TODO_1: Estimate the expected posterior probabilities.
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    posterieor_probs = zeros(256, 4);
    
    for intensity = 1:256
        intensity_given_weight = zeros(4, 1);        
        p_intensity = 0;
        
        for i = 1:4
            mu = Mu(i);
            sigma = Sigma(i);
            weight = Weight(i);
            
            intensity_given_weight(i) = normpdf(intensity,mu,sigma)*weight;
            p_intensity = p_intensity + intensity_given_weight(i);
        end
        
        for i = 1:4
            posterior_probs(intensity, i) = intensity_given_weight(i)/p_intensity;
        end
    end
    
    
	% Allocate spaces for the parameters estimated.
	NewWeight = zeros(4,1);
	NewMu = zeros(4,1);
	NewSigma = zeros(4,1);
    
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% TODO_2: Estimate the parameters.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for i = 1:4
        sigma_numerator = 0;
        mu_numerator = 0;
        sum_posteriors = 0;
        for intensity = 1:256
            posterior_prob = posterior_probs(intensity, i);
            
            mu_numerator = mu_numerator + posterior_prob*intensity;
            sum_posteriors = sum_posteriors + posterior_prob;
        end
        
        NewMu(i) = mu_numerator/sum_posteriors;
        
        for intensity = 1:256
            posterior_prob = posterior_probs(intensity, i);
            sigma_numerator = sigma_numerator + posterior_prob * (intensity - NewMu(i))^2;
        end
        
        NewSigma(i) = sqrt(sigma_numerator/sum_posteriors);
        NewWeight(i) = 1/9.0 * sum_posteriors;
    end
	
    % Check if convergence is reached.
	DiffWeight = abs(NewWeight-Weight)./Weight;
	DiffMu = abs(NewMu-Mu)./Mu;
	DiffSigma = abs(NewSigma-Sigma)./Sigma;
	
	if (max(DiffWeight) < threshold) & (max(DiffMu) < threshold) & (max(DiffSigma) < threshold)
        break;
	end
	
	% Update the parameters.
	Weight = NewWeight;
	Mu = NewMu;
	Sigma = NewSigma;
    
    disp(['Iteration #' num2str(itn)]);
    disp([' Weight: ' num2str(Weight(1)) ' ' num2str(Weight(2)) ' ' num2str(Weight(3)) ' ' num2str(Weight(4))]);
    disp([' Mu: ' num2str(Mu(1)) ' ' num2str(Mu(2)) ' ' num2str(Mu(3)) ' ' num2str(Mu(4))]);
    disp([' Sigma: ' num2str(Sigma(1)) ' ' num2str(Sigma(2)) ' ' num2str(Sigma(3)) ' ' num2str(Sigma(4))]);
    itn = itn + 1;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO_3(a): Compute minimum error threshold between the first and the second
% Gaussian distributions.
%
% FirstThreshold = ?
FirstThreshold = min_error_fancy(Mu(1), Mu(2), Sigma(1), Sigma(2), Weight(1), Weight(2));
 
% TODO_3(b): Compute minimum error threshold between the second and the third
% Gaussian distributions.
%
% SecondThreshold = ?
SecondThreshold = min_error_fancy(Mu(2), Mu(3), Sigma(2), Sigma(3), Weight(2), Weight(3));

% TODO_3(c): Compute minimum error threshold between the third and the fourth
% Gaussian distributions.
%
% ThirdThreshold = ?
ThirdThreshold = min_error_fancy(Mu(3), Mu(4), Sigma(3), Sigma(4), Weight(3), Weight(4));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Show the segmentation results.
figure;
subplot(2,3,1);imshow(Im);title('Panda');
subplot(2,3,3);imshow(Im<=FirstThreshold);title('First Intensity Class');
subplot(2,3,4);imshow(Im>FirstThreshold & Im<SecondThreshold);title('Second Intensity Class');
subplot(2,3,5);imshow(Im>SecondThreshold & Im<ThirdThreshold);title('Third Intensity Class');
subplot(2,3,6);imshow(Im>=ThirdThreshold);title('Fourth Intensity Class');
Params = zeros(12,1);
Params(1) = Weight(1);
Params(2) = Mu(1);
Params(3) = Sigma(1);
Params(4) = Weight(2);
Params(5) = Mu(2);
Params(6) = Sigma(2);
Params(7) = Weight(3);
Params(8) = Mu(3);
Params(9) = Sigma(3);
Params(10) = Weight(4);
Params(11) = Mu(4);
Params(12) = Sigma(4);
subplot(2,3,2);ggg(Params,Hist);
end

function p = gaussian(mu, sigma, z)
    mul = 1/(sqrt(pi*2) * sigma);
    pow = -(z - mu)^2/(2*sigma^2);
    
    p = exp(pow)*mul;
end

function error = min_error(mu1, mu2, sigma1, sigma2, weight1, weight2)
    mu = (mu1 + mu2)/2;
    weight = log(weight2/weight1);
    mul = sigma1^2/(mu1 - mu2);
    
    error = mu + mul*weight;
end

function error = min_error_fancy(mu1, mu2, sigma1, sigma2, weight1, weight2)
  a = 1/(2*sigma1^2) - 1/(2*sigma2^2);
  b = mu2/(sigma2^2) - mu1/(sigma1^2);
  c = mu1^2 /(2*sigma1^2) - mu2^2 / (2*sigma2^2) - log(sigma2/sigma1);
  
  abc = [a,b,c];
  
  r = roots(abc);
  error = r(find(r >= 0 & r <= 255));
end