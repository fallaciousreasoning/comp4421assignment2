def dilate(image, mask):
	height = len(image)
	width = len(image[0])

	mask_height = len(mask)
	mask_width = len(mask[0])

	result = [[0] * width for i in range(height)];

	for y in range(height):
		for x in range(width):			
			if image[y][x] == 0: continue
			result[y][x] = 1

			for i in range(mask_height):
				for j in range(mask_width):
					if mask[i][j] == 0: continue

					y_index = y - mask_height//2 + i
					x_index = x - mask_width//2 + j

					if x_index < 0 or y_index < 0 or y_index >= height or x_index >= width: continue
					result[y_index][x_index] = 1

	return result

def erode(image, mask):
	height = len(image)
	width = len(image[0])

	mask_height = len(mask)
	mask_width = len(mask[0])

	result = [[1] * width for i in range(height)];

	for y in range(height):
		for x in range(width):			
			if image[y][x] == 1: continue

			result[y][x] = 0

			for i in range(mask_height):
				for j in range(mask_width):
					if mask[i][j] == 0: continue

					y_index = y - mask_height//2 + i
					x_index = x - mask_width//2 + j

					if x_index < 0 or y_index < 0 or y_index >= height or x_index >= width: continue
					result[y_index][x_index] = 0

	return result

def print_image(image):
	print("[")

	for i in range(len(image)):
		print("\t" + str(image[i]) + ",")

	print("]")


image = [
	[0]*8,
	[0,0,0,1,1,1,1,0],
	[0,0,0,1,1,1,1,0],
	[0,1,1,1,1,1,1,0],
	[0,1,1,1,1,1,1,0],
	[0,1,1,1,1,0,0,0],
	[0,1,1,1,1,0,0,0],
	[0]*8,
]

mask_1 = [
	[1,1,1],
	[1,1,1],
	[1,1,1],
]

mask_2 = [
	[0,1,0],
	[1,1,1],
	[0,1,0],
]

print("Dilation Mask 1")
print_image(dilate(image, mask_1))
print()

print("Dilation Mask 2")
print_image(dilate(image, mask_2))
print()

print("Erosion Mask 1")
print_image(erode(image, mask_1))
print()

print("Erosion Mask 2")
print_image(erode(image, mask_2))
print()
